#define _XOPEN_SOURCE 500
#include <unistd.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>

#define WIDTH 60
#define HEIGHT 60
#define FPS 30

typedef struct {
	int x;
	int y;
} V2i;

typedef struct {
	float x;
	float y;
} V2f;

typedef struct {
	float x;
	float y;
	int r;
	V2f dir;
} Ball;

typedef enum {
	NONE,
	FULL
} Pixel;

static Pixel buffer[WIDTH*HEIGHT];

void clear() {
	for (int i = 0; i < WIDTH*HEIGHT; ++i) {
		buffer[i] = NONE;
	}

	printf("\x1b[%dD", WIDTH);
	printf("\x1b[%dA", HEIGHT);
}

void putChar(char c, int x, int y) {
	buffer[x+y*WIDTH] = c;
}

void drawBall(Ball* ball) {
	for (int y = 0; y < HEIGHT; ++y) {
		for (int x = 0; x < WIDTH; ++x) {
			float diffX = x-ball->x;
			float diffY = y-ball->y;
			if (sqrt(diffX*diffX + diffY*diffY) < ball->r) buffer[x+y*WIDTH] = FULL;
		}
	}
}

void draw() {
	for (int y = 0; y < HEIGHT; y += 2) {
		char line[WIDTH];
		char map[2][2] = {
			{' ', '_'},
			{'^', 'C'}
		};
		for (int x = 0; x < WIDTH; ++x) {
			line[x] = map[buffer[x+y*WIDTH]][buffer[x+(y+1)*WIDTH]];
		}
		printf("%s\n", line);
	}
}

Ball createBall(float x, float y, int r, float dirX, float dirY) {
	Ball ball;
	ball.x = x;
	ball.y = y;
	ball.r = r;
	ball.dir.x = dirX;
	ball.dir.y = dirY;

	return ball;
}

int main() {
	int ballCount = 0;
	Ball* balls = malloc(sizeof(Ball));
	balls[ballCount++] = createBall(WIDTH/2.f, HEIGHT/2.f, 10, .8f, .2f);

	while (1) {
		clear();
		usleep(1000 * 1000/FPS);

		int b = ballCount;
		for (int i = 0; i < b; ++i) {
			Ball* ball = &balls[i];
			ball->x += ball->dir.x*2;
			ball->y += ball->dir.y*2;

			if (ball->x+ball->r > WIDTH) {
				ball->x = WIDTH-ball->r;
				ball->dir.x *= -1;
				ball->r--;
				balls = realloc(balls, (ballCount+1)*sizeof(Ball));
				ball = &balls[i];
				balls[ballCount++] = createBall(ball->x, ball->y, ball->r, ball->dir.x, -ball->dir.y);
			} else if (ball->x-ball->r < 0) {
				ball->x = ball->r;
				ball->dir.x *= -1;
				ball->r--;
				balls = realloc(balls, (ballCount+1)*sizeof(Ball));
				ball = &balls[i];
				balls[ballCount++] = createBall(ball->x, ball->y, ball->r, ball->dir.x, -ball->dir.y);
			}
			if (ball->y+ball->r > HEIGHT) {
				ball->y = HEIGHT-ball->r;
				ball->dir.y *= -1;
				ball->r--;
				balls = realloc(balls, (ballCount+1)*sizeof(Ball));
				ball = &balls[i];
				balls[ballCount++] = createBall(ball->x, ball->y, ball->r, -ball->dir.x, ball->dir.y);
			} else if (ball->y-ball->r < 0) {
				ball->y = ball->r;
				ball->dir.y *= -1;
				ball->r--;
				balls = realloc(balls, (ballCount+1)*sizeof(Ball));
				ball = &balls[i];
				balls[ballCount++] = createBall(ball->x, ball->y, ball->r, -ball->dir.x, ball->dir.y);
			}

			drawBall(ball);
		}

		draw();
	}

	free(balls);
}
