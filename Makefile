CC = cc
CFLAGS = -std=c99 -Wall -Wextra -pedantic
LDFLAGS = -lm

balls: *.c
	$(CC) $(CFLAGS) -o balls main.c $(LDFLAGS)

test: balls
	./balls
